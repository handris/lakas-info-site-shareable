import { I18n } from '@ngx-translate/i18n-polyfill';

export interface SelectableOption {
  visibleValue: string,
  modelInput: string
}

export class FormMappings {

  constructor(private i18n: I18n) { }

  floors: SelectableOption[] = [
  {visibleValue: this.i18n('Földszint'), modelInput: '0'},
  {visibleValue: this.i18n('Félemelet'), modelInput: '0.5'},
  {visibleValue: this.i18n('Szuterén'), modelInput: '-1'},
  {visibleValue: '1', modelInput: '1'},
  {visibleValue: '2', modelInput: '2'},
  {visibleValue: '3', modelInput: '3'},
  {visibleValue: '4', modelInput: '4'},
  {visibleValue: '5', modelInput: '5'},
  {visibleValue: '6', modelInput: '6'},
  {visibleValue: '7', modelInput: '7'},
  {visibleValue: '8', modelInput: '8'},
  {visibleValue: '9', modelInput: '9'},
  {visibleValue: '10', modelInput: '10'},
  {visibleValue: this.i18n('10 felett'), modelInput: '11'},
  ];

  years: SelectableOption[] = [
    {visibleValue: this.i18n('1950 előtt'), modelInput: 'Before_1950'},
    {visibleValue: '1950 - 1980', modelInput: '1950-1980'},
    {visibleValue: '1981 - 2000', modelInput: '1981-2000'},
    {visibleValue: '2001 - 2010', modelInput: '2001-2010'},
    {visibleValue: '2011 - 2017', modelInput: '2011-2017'},
    {visibleValue: this.i18n('2017 után'), modelInput: 'After_2017'},
  ];

  conditions: SelectableOption[] = [
    {visibleValue: this.i18n('Újszerű'), modelInput: 'newlike'},
    {visibleValue: this.i18n('Jó állapotú'), modelInput: 'good'},
    {visibleValue: this.i18n('Felújított'), modelInput: 'renovated'},
    {visibleValue: this.i18n('Közepes állapotú'), modelInput: 'mediocre'},
    {visibleValue: this.i18n('Felújítandó'), modelInput: 'bad'},
    {visibleValue: this.i18n('Új építésű'), modelInput: 'newlybuilt'},
    {visibleValue: this.i18n('Befejezetlen'), modelInput: 'unfinished'},
    {visibleValue: this.i18n('Egyéb'), modelInput: 'NA'},
  ];

  comforts: SelectableOption[] = [
    {visibleValue: this.i18n('Összkomfortos'), modelInput: 'full'},
    {visibleValue: this.i18n('Duplakomfortos'), modelInput: 'double'},
    {visibleValue: this.i18n('Luxus'), modelInput: 'luxury'},
    {visibleValue: this.i18n('Komfortos'), modelInput: 'normal'},
    {visibleValue: this.i18n('Félkomfortos'), modelInput: 'half'},
    {visibleValue: this.i18n('Komfort nélküli'), modelInput: 'lack'},
    {visibleValue: this.i18n('Egyéb'), modelInput: 'NA'},
  ];

  elevators: SelectableOption[] = [
    {visibleValue: this.i18n('Van'), modelInput: 'yes'},
    {visibleValue: this.i18n('Nincs'), modelInput: 'no'},
  ];

  heights: SelectableOption[] = [
    {visibleValue: this.i18n('3 méternél alacsonyabb'), modelInput: 'below3m'},
    {visibleValue: this.i18n('3 méter vagy magasabb'), modelInput: 'over3m'},
  ];

  bathrooms: SelectableOption[] = [
    {visibleValue: this.i18n('Külön helyiségben'), modelInput: 'külön helyiségben'},
    {visibleValue: this.i18n('Egy helyiségben'), modelInput: 'egy helyiségben'},
    {visibleValue: this.i18n('Külön és egyben is'), modelInput: 'külön és egyben is'},
    {visibleValue: this.i18n('Egyéb'), modelInput: 'NA'},
  ];

  views: SelectableOption[] = [
    {visibleValue: this.i18n('Utca'), modelInput: 'street'},
    {visibleValue: this.i18n('Udvar'), modelInput: 'backyard'},
    {visibleValue: this.i18n('Kertre néző'), modelInput: 'garden'},
    {visibleValue: this.i18n('Panorámás'), modelInput: 'panoramic'},
    {visibleValue: this.i18n('Egyéb'), modelInput: 'NA'},
  ];

  parkings: SelectableOption[] = [
    {visibleValue: this.i18n('Utca, közterület'), modelInput: 'public'},
    {visibleValue: this.i18n('Utca, közterület - ingyenes'), modelInput: 'public_free'},
    {visibleValue: this.i18n('Teremgarázs hely - megvásárolható'), modelInput: 'garage_canbuy'},
    {visibleValue: this.i18n('Utca, közterület - fizetős övezet'), modelInput: 'public_fee'},
    {visibleValue: this.i18n('Udvari beálló - benne van az árban'), modelInput: 'yard_included'},
    {visibleValue: this.i18n('Önálló garázs - megvásárolható'), modelInput: 'standalone_canbuy'},
    {visibleValue: this.i18n('Önálló garázs - benne van az árban'), modelInput: 'standalone_included'},
    {visibleValue: this.i18n('Teremgarázs hely - benne van az árban'), modelInput: 'garage_included'},
    {visibleValue: this.i18n('Teremgarázs hely'), modelInput: 'garage'},
    {visibleValue: this.i18n('Teremgarázs hely - kötelező megvenni'), modelInput: 'garage_mustbuy'},
    {visibleValue: this.i18n('Udvari beálló'), modelInput: 'yard'},
    {visibleValue: this.i18n('Udvari beálló - megvásárolható'), modelInput: 'yard_canbuy'},
    {visibleValue: this.i18n('Egyéb'), modelInput: 'NA'},
  ];

  airconditionings: SelectableOption[] = [
    {visibleValue: this.i18n('Van'), modelInput: 'yes'},
    {visibleValue: this.i18n('Nincs'), modelInput: 'no'},
  ];

  heatings: SelectableOption[] = [
    {visibleValue: this.i18n('Gáz (cirko)'), modelInput: 'gas_central'},
    {visibleValue: this.i18n('Gáz (konvektor)'), modelInput: 'gas_convector'},
    {visibleValue: this.i18n('Házközponti egyedi méréssel'), modelInput: 'central_individual'},
    {visibleValue: this.i18n('Távfűtés'), modelInput: 'district'},
    {visibleValue: this.i18n('Házközponti'), modelInput: 'central'},
    {visibleValue: this.i18n('Távfűtés egyedi méréssel'), modelInput: 'district_individual'},
    {visibleValue: this.i18n('Elektromos'), modelInput: 'electric'},
    {visibleValue: this.i18n('Hőszivattyú'), modelInput: 'heat_pump'},
    {visibleValue: this.i18n('Megújuló energia'), modelInput: 'renewable'},
    {visibleValue: this.i18n('Cserépkályha'), modelInput: 'masonry_heater'},
    {visibleValue: this.i18n('Hőszivattyú'), modelInput: 'heat_pump'},
    {visibleValue: this.i18n('Gáz (cirko), padlófűtés'), modelInput: 'gas_underfloor'},
    {visibleValue: this.i18n('Fan-coil'), modelInput: 'fan_coil'},
    {visibleValue: this.i18n('Gázkazán'), modelInput: 'gas_central'},
    {visibleValue: this.i18n('Egyéb'), modelInput: 'NA'},
  ];

}

