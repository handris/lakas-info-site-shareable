export class PredictionRequest {
  area: number;
  district: number;
  floor: string;
  half_rooms: number;
  price: number;
  teras: number;
  whole_rooms: number;
  year: string;
  condition: string;
  comfort: string;
  elevator: string;
  height: string;
  heating: string;
  bathroom: string;
  view: string;
  parking: string;
  airconditioning: string;

  constructor(area, teras) {}
}
