import {Injectable, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable()
export class PredictionService implements OnInit{
  headers: HttpHeaders;

  constructor(private http: HttpClient){}

  ngOnInit(): void {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getPriceFromInputData(requestBody) {
    const baseUrl = '/api/predict-from-inputs';

    return this.http.post(baseUrl, requestBody, {headers: this.headers})
      .pipe(map((response: any) => response));
  }
}
