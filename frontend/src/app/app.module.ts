import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import { FormsModule }   from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from "./component/app.component";
import {AboutUsComponent} from "./component/about-us/about-us.component";
import {FooterComponent} from "./component/footer/footer.component";
import {NavbarComponent} from "./component/navbar/navbar.component";
import {EvaluationComponent} from "./component/evaluation/evaluation.component";
import {PredictionService} from "./service/prediction.service";
import {EvaluationResponseComponent} from "./component/evaluation-response/evaluation-response.component";
import {DashboardComponent} from "./component/dashboard/dashboard.component";
import { TRANSLATIONS, TRANSLATIONS_FORMAT } from '@angular/core';
import { I18n } from '@ngx-translate/i18n-polyfill';

const appRoutes: Routes = [
  { path: '', component: EvaluationComponent },
  { path: 'evaluation', component: EvaluationComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'about-us' , component: AboutUsComponent},
  { path: '**', component: EvaluationComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    EvaluationComponent,
    EvaluationResponseComponent,
    FooterComponent,
    AboutUsComponent,
    NavbarComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    I18n,
    {
      provide: TRANSLATIONS,
      useFactory: locale => require(`raw-loader!../i18n/messages.${locale === 'en-US' ? 'hu' : locale}.xlf`),
      deps: [LOCALE_ID]
    },
    {provide: TRANSLATIONS_FORMAT, useValue: 'xlf'},
    PredictionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
