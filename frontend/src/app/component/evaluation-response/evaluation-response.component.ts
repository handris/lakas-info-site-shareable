import {OnChanges, Component, EventEmitter, Output, Input, SimpleChanges, HostListener} from '@angular/core';
import {PredictionRequest} from "../../model/prediction-request";

@Component({
  selector: 'evaluation-response',
  templateUrl: './evaluation-response.component.html',
  styleUrls: ['./evaluation-response.component.css']
})
export class EvaluationResponseComponent implements OnChanges {
  @Input('predictedPrice') predictedPrice: number = null;
  @Output() shouldGoBack = new EventEmitter<boolean>();
  prediction: PredictionRequest;
  onSmallScreen: boolean;
  innerWidth: number;

  @HostListener('window:resize', ['$event'])
  ngOnInit(event) {
      this.checkWindowSize(event);

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkWindowSize(event);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.prediction = changes.predictedPrice.currentValue;
  }

  clickBack() {
    this.shouldGoBack.emit(true);
  }

  @HostListener('window:resize', ['$event'])
  checkWindowSize(event) {
    this.innerWidth = window.innerWidth;
    if (innerWidth< 993) {
      this.onSmallScreen = true;
    } else {
      this.onSmallScreen = false;
    }
  }
}
