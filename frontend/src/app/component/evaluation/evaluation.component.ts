import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from "@angular/core";
import * as M from 'materialize-css';
import {NgForm} from "@angular/forms";
import {PredictionService} from "../../service/prediction.service";
import {PredictionRequest} from "../../model/prediction-request";
import {FormMappings, SelectableOption} from "../../model/form-mappings";
import { I18n } from '@ngx-translate/i18n-polyfill';

@Component({
  selector: 'app-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.css']
})
export class EvaluationComponent implements OnInit, AfterViewInit {

  @ViewChild('f') predictionForm: NgForm;
  districts: number[];
  floors: SelectableOption[];
  years: SelectableOption[];
  conditions: SelectableOption[];
  comforts: SelectableOption[];
  elevators: SelectableOption[];
  heights: SelectableOption[];
  bathrooms: SelectableOption[];
  views: SelectableOption[];
  parkings: SelectableOption[];
  airconditionings: SelectableOption[];
  heatings: SelectableOption[];
  isPredictionComplete: boolean = false;
  predictionRequest: PredictionRequest;
  formMappings: FormMappings;
  predictionResponse: number;
  initialArea = 175;
  initialTerasArea = 0;
  hasTeras: boolean;

  onSmallScreen: boolean;
  innerWidth: number;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkWindowSize(event);
  }

  constructor(private predictionService: PredictionService, private elementRef:ElementRef, private i18n: I18n) {}

  @HostListener('window:resize', ['$event'])
  ngOnInit(): void {
    this.checkWindowSize(event);
    this.formMappings = new FormMappings(this.i18n);
    this.predictionRequest = new PredictionRequest(this.initialArea, this.initialTerasArea);
    this.districts = Array(23).fill(null, 0).map((_, i) => i + 1);
    this.floors = this.formMappings.floors;
    this.years = this.formMappings.years;
    this.comforts = this.formMappings.comforts;
    this.elevators = this.formMappings.elevators;
    this.heights = this.formMappings.heights;
    this.bathrooms = this.formMappings.bathrooms;
    this.views = this.formMappings.views;
    this.parkings = this.formMappings.parkings;
    this.airconditionings = this.formMappings.airconditionings;
    this.conditions = this.formMappings.conditions;
    this.heatings = this.formMappings.heatings;
  }

  ngAfterViewInit(): void {
    M.updateTextFields();
    const elements = this.elementRef.nativeElement.querySelectorAll('select');
    M.FormSelect.init(elements, '');
    const elems  = document.querySelectorAll("input[type=range]");
    M.Range.init(elems);
    document.addEventListener('DOMContentLoaded', function() {
    console.log(this.predictionForm);
  });

  }

  initiateSearch() {
    const requestBody = {
      'district': this.predictionRequest.district,
      'area': this.predictionRequest.area || this.initialArea,
      'rooms': this.predictionRequest.whole_rooms,
      'floor': this.predictionRequest.floor,
      'teras': this.predictionRequest.teras || this.initialTerasArea,
      'half_rooms': this.predictionRequest.half_rooms,
      'year': this.predictionRequest.year || 'NA',
      'comfort': this.predictionRequest.comfort || 'NA',
      'elevator': this.predictionRequest.elevator || 'NA',
      'height': this.predictionRequest.height || 'NA',
      'bathroom': this.predictionRequest.bathroom || 'NA',
      'view': this.predictionRequest.view || 'NA',
      'parking': this.predictionRequest.parking || 'NA',
      'airconditioning': this.predictionRequest.airconditioning || 'NA',
      'condition': this.predictionRequest.condition || 'NA',
      'heating': this.predictionRequest.heating || 'NA'
    };

    this.predictionService.getPriceFromInputData(requestBody).subscribe(response => {
      this.isPredictionComplete = true;
      this.predictionResponse = response.price;
    });
  }

  onBackClick(shouldGoBack: boolean) {
    if (shouldGoBack) { this.isPredictionComplete = false; }
  }

  @HostListener('window:resize', ['$event'])
  checkWindowSize(event) {
    this.innerWidth = window.innerWidth;
    if (innerWidth< 993) {
      this.onSmallScreen = true;
    } else {
      this.onSmallScreen = false;
    }
  }

  getWidth() {

    const inputs = [this.predictionRequest.district,
      this.predictionRequest.area,
      this.predictionRequest.whole_rooms,
      this.predictionRequest.floor,
      this.predictionRequest.teras,
      this.predictionRequest.half_rooms,
      this.predictionRequest.year,
      this.predictionRequest.comfort,
      this.predictionRequest.elevator,
      this.predictionRequest.height,
      this.predictionRequest.bathroom,
      this.predictionRequest.view,
      this.predictionRequest.parking,
      this.predictionRequest.airconditioning,
      this.predictionRequest.condition];
    let completed = 0;
    for (let i = 0; i < inputs.length; i++) {

      if (inputs[i] != null) { completed += 100/(inputs.length); }

    }
    return completed + "%";
  }
}
