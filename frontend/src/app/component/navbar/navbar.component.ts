import {AfterViewInit, Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import * as Materialize from 'materialize-css';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements AfterViewInit {

  constructor(private element: ElementRef) { }

  onSmallScreen: boolean;
  innerWidth: number;

  ngAfterViewInit() {
    const sideNavs = this.element.nativeElement.querySelectorAll('.sidenav');
    Materialize.Sidenav.init(sideNavs, null);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkWindowSize(event);
  }

  @ViewChild('navbar') navBar: ElementRef;

  @HostListener('window:resize', ['$event'])
  ngOnInit(): void {
    this.checkWindowSize(event);
  }

  @HostListener('window:resize', ['$event'])
  checkWindowSize(event) {
    this.innerWidth = window.innerWidth;
    if (innerWidth< 993) {
      this.onSmallScreen = true;
    } else {
      this.onSmallScreen = false;
    }
  }

  @HostListener('window:scroll')
  onScroll() {
    if (!this.onSmallScreen) {
      if (window.scrollY > 200) {
        this.navBar.nativeElement.classList.remove('transparent');
        this.navBar.nativeElement.classList.add('green-background');
      } else {
        this.navBar.nativeElement.classList.add('transparent');
        this.navBar.nativeElement.classList.remove('green-background');
      }
    }
  }

}
