### Run the project backend locally:
* in the project root run: `make up`


### Run the frontend with Hungarian locale:
* in the `frontend` folder run `npm start`


### Create an English translation:
* find an HTML tag to translate and add the attribute `i18n` to it (you may pass [values](https://angular.io/guide/i18n#help-the-translator-with-a-description-and-meaning) to the attribute that helps the 
translator)
```html
<h4 i18n>Üdvözöljük a Lakás.info-n!</h4>
```
* extract texts to be translated by `npm run extract`
* find the extracted file in `frontend/src/locale/messages.xlf`
* make a copy of this file in the same folder and name it `messages.en.xlf`
* open `messages.en.xlf`, where each html tag assigned for translation will be contained in a `<trans-unit>` tag
* make a translation by creating a `<target>` tag inside each `<trans-unit>` tag with the translation inside:
```html
<trans-unit id="introductionHeader" datatype="html">
  <source>Üdvözöljük a Lakás.info-n!</source>
  <target>Welcome to Lakas.info</target>
  <context-group purpose="location">
    <context context-type="sourcefile">app/component/index/index.component.html</context>
    <context context-type="linenumber">7</context>
  </context-group>
</trans-unit>
```
* look at the site with its newly translated content by running `npm run start:en`