def generate_scraping_start_email_as_html(message, time):
    return """\
    <html>
      <head></head>
      <body>
        <p>{} {}</p>
        <p>You can check the logs <a href="https://app.logz.io/#/dashboard/kibana">here</a></p>
      </body>
    </html>
    """.format(message, time)


def generate_scraping_start_email_as_text(message, time):
    return "{} {}. Check the logs at https://app.logz.io/#/dashboard/kibana."\
        .format(message, time)
