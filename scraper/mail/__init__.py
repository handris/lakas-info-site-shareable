from mail.mail import send_mail
from mail.mail import notify_scraping_start, notify_scraping_end, notify_scraping_error
from mail.template import generate_scraping_start_email_as_html, generate_scraping_start_email_as_text

from utils.env_check import check_if_env_vars_are_set


check_if_env_vars_are_set(['NOTIFIER_EMAIL', 'NOTIFIER_EMAIL_PASSWORD', 'NOTIFICATION_TARGET_EMAIL'])
