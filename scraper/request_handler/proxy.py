import os
import requests
from request_handler.user_agent import get_random_user_agent
import json
from time import sleep
import logging

logger = logging.getLogger(__name__)

url = 'http://falcon.proxyrotator.com:51337/'
params = dict(
    apiKey=os.getenv('PROXY_ROTATOR_API_KEY'),
    country='HU'
)
target_url = 'https://ingatlan.com'


class ThereAreNoAvailableProxiesError(Exception):
    pass


def get_valid_proxy():
    while True:
        try:
            resp = requests.get(url=url, params=params)
            if 'error' in json.loads(resp.text):
                raise ThereAreNoAvailableProxiesError()
        except ThereAreNoAvailableProxiesError:
            logger.info("There are no available proxies at the moment. Waiting for a while...")
            sleep(10)
            continue

        proxy = json.loads(resp.text)['proxy']
        user_agent = get_random_user_agent()
        proxies = {
            'http': 'http://{}'.format(proxy),
            'https': proxy
        }

        try:
            response = requests.get(target_url,
                                    timeout=2,
                                    headers={'User-Agent': user_agent},
                                    proxies=proxies)
            if response.status_code == 200:
                return proxy, user_agent
        except requests.exceptions.ConnectionError:
            logger.info("Current proxy does not work, getting a new one...")
        except ConnectionResetError:
            logger.info("The server closed the connection. We should wait for a while...")
            sleep(15)
        except requests.exceptions.ReadTimeout:
            logger.info("Request to {} timed out. We should get a new proxy...".format(target_url))
