from request_handler.request import RequestAdapter
from request_handler.user_agent import get_random_user_agent

from utils.env_check import check_if_env_vars_are_set


check_if_env_vars_are_set(['PROXY_ROTATOR_API_KEY'])
