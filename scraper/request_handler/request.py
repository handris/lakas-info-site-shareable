import requests
from urllib3.exceptions import MaxRetryError

from request_handler.proxy import get_valid_proxy
from requests import Session
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from time import sleep
from random import random
from logging import getLogger

REQUEST_DELAY_RANGE = (0.5, 1.5)
logger = getLogger(__name__)


class RequestAdapter:

    def __init__(self):
        self.proxy, self.user_agent = get_valid_proxy()
        logger.info("Got new proxy {} and user-agent {}".format(self.proxy, self.user_agent))
        self.session = Session()

        retries = Retry(total=5, backoff_factor=1)
        self.session.mount('https://', HTTPAdapter(max_retries=retries))

    def make_request(self, url):
        sleep(REQUEST_DELAY_RANGE[0] + (REQUEST_DELAY_RANGE[1] - REQUEST_DELAY_RANGE[0]) * random())
        proxies = {
            'http': 'http://{}'.format(self.proxy),
            'https': self.proxy
        }
        try:
            response = self.session.get(url, headers={'User-Agent': self.user_agent}, proxies=proxies, timeout=5)

            if response.status_code == 403:
                logger.info("Received 403. Renewing proxy then retyring...")
                self.refresh_session()
                return self.session.get(url, headers={'User-Agent': self.user_agent}, proxies=proxies)

            logger.info("Response status: {}".format(response.status_code))
            return response
        except MaxRetryError:
            logger.info("Max retries exceeded with url '{}'".format(url))
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            logger.info("Request to {} timed out. We should get a new proxy...".format(url))
            self.refresh_session()
            return self.make_request(url)

    def refresh_session(self):
        self.proxy, self.user_agent = get_valid_proxy()
        self.session = Session()

        retries = Retry(total=5, backoff_factor=1)
        self.session.mount('https://', HTTPAdapter(max_retries=retries))
        logger.info("Got new proxy {} and user-agent {} and created new session.".format(self.proxy, self.user_agent))
