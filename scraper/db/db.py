import os
import logging
from pymongo import MongoClient

logger = logging.getLogger(__name__)

mongo = MongoClient("mongodb://" + os.getenv('MONGO_USER') + ":" + os.getenv('MONGO_PASSWORD') + "@mongo")


def get_real_estate_records_by_period(period):
    logger.info("get_real_estate_records_by_period() called with period {}".format(period))
    return [document for document in mongo.real_estates.real_estate_records.find({"_id.time": period})]


def get_real_estate_records_by_period_and_district(period, district):
    logger.info("get_real_estate_records_by_period_and_district() called with period {} and district {}"
                .format(period, district))
    return [document for document in mongo.real_estates.real_estate_records.find(
        {"_id.time": period, "District": district}
    )]


def get_existing_time_periods():
    logger.info("get_existing_time_periods called")
    return [document for document in mongo.real_estates.real_estate_records.distinct("_id.time")]


class DbClient:
    connection_url = "mongodb://" + os.getenv('MONGO_USER') + ":" + os.getenv('MONGO_PASSWORD') + "@mongo"

    def __init__(self):
        self.client = MongoClient(DbClient.connection_url)

    def save_real_estate_record(self, record):
        try:
            self.client.real_estates.real_estate_records.insert(record)
        except Exception as exception:
            logger.error("An exception occured while persisting record with id {} and url {}. Exception: {}"
                         .format(record["id"], record["URL"], exception))
