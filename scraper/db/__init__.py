from utils.env_check import check_if_env_vars_are_set

from db.db import DbClient, get_real_estate_records_by_period
from db.db import get_real_estate_records_by_period_and_district, get_existing_time_periods


check_if_env_vars_are_set(['MONGO_USER', 'MONGO_PASSWORD'])
