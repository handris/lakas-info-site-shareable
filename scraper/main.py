import datetime
import logging
from multiprocessing import Pool

from db import DbClient
from mail import notify_scraping_start, notify_scraping_end, notify_scraping_error
from request_handler import RequestAdapter
from scraping_jobs import extract_number_of_listing_pages, extract_links_from_listing_page
from scraping_jobs import generate_parent_pages, extract_real_estate_document

logger = logging.getLogger(__name__)


def scrape_district(district_url, district_number, time_identifier):
    logger.info("Starting scraping district url {}".format(district_url))
    mongo = DbClient()

    http = RequestAdapter()
    response = http.make_request(district_url)
    logger.info("Scraped parent page of district {} with status {}".format(district_number, response.status_code))

    try:
        number_of_listing_pages = extract_number_of_listing_pages(response.content)
    except TypeError:
        logger.error("TypeError when extracting the number of listing pages!\nResponse: {}\nResponse.status{}"
                     .format(response.content, response.status_code))
        notify_scraping_error(district_number)
        return

    links_page_page = []
    for page in range(1, number_of_listing_pages + 1):
        link = "{}?page={}".format(district_url, page)
        response = http.make_request(link)
        logger.info("Scraped listing page {} with status ".format(link, response.status_code))
        links_page_page.append(extract_links_from_listing_page(response.content))

    for href in [link for sublist in links_page_page for link in sublist]:
        response = http.make_request(href)

        if response.status_code != 200:
            document = {"error": "Url is no longer available"}
            logger.info("Document with href {} is no longer available.".format(href))
        else:
            document = extract_real_estate_document(
                response.content, district_number, time_identifier, response.request.path_url
            )
            logger.info("Successfully scraped document with url {}".format(document.get("URL")))
        mongo.save_real_estate_record(document)


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(process)d - %(module)s - %(name)s - [%(levelname)s] - %(message)s'
    )
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    logger.info("Starting scraping....")

    now = datetime.datetime.now().isocalendar()
    time_identifier = str(now[0]) + "_" + str(now[1])

    notify_scraping_start()

    pool = Pool(5)
    pool.starmap(scrape_district, [
        (page.get('url'), page.get('district_number'), time_identifier)
        for page in generate_parent_pages()])

    pool.close()
    pool.join()

    notify_scraping_end()
    logger.info("______________________________________________________")
    logger.info("Scraping session ended at {}".format(datetime.datetime.now()))
    logger.info("Scraping duration: {}".format(datetime.datetime.now() - now))
    logger.info("______________________________________________________")
    logger.info("______________________________________________________")



