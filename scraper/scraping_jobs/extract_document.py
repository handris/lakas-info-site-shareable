from bs4 import BeautifulSoup
import logging


logger = logging.getLogger(__name__)


DEFAULT_TAG = BeautifulSoup(features='html.parser').new_tag(name="p")
DEFAULT_TAG.string = "NA"


def extract_real_estate_document(html_content, district=None, time_identifier=None, url=None):
    page = BeautifulSoup(html_content, 'html.parser')
    table = page.find("div", class_="paramterers")

    def get_transport_type(tag): return tag.find("span", class_="public-transport-name").text

    def get_routes(tag): return [a.text for a in tag.find("div", class_="public-transport-routes").find_all("a")]

    def extract_from_table(table, text):
        node = table.find("td", text=text)
        return "NA" if not node else node.find_next_sibling("td").text

    return {
        "ID": (page.find("b", class_="listing-id") or DEFAULT_TAG).text,
        "Address": (page.find("h1", class_="js-listing-title") or DEFAULT_TAG).text,
        "Agent": (page.find("div", class_="agent-name") or DEFAULT_TAG).text,
        "Office": (page.find("span", class_="office-name") or DEFAULT_TAG).text,
        "Area": ((page.find("div", {"class": "parameter parameter-area-size"}) or DEFAULT_TAG)
                 .find("span", class_="parameter-value") or DEFAULT_TAG).text,
        "Rooms": ((page.find("div", {"class": "parameter parameter-room"}) or DEFAULT_TAG)
                  .find("span", class_="parameter-value") or DEFAULT_TAG).text,
        "Price": ((page.find("div", {"class": "parameter parameter-price"}) or DEFAULT_TAG)
                  .find("span", class_="parameter-value") or DEFAULT_TAG).text,
        "Transports": [{get_transport_type(group): get_routes(group)}
                       for group in page.find_all("div", class_="public-transport-group")],
        "Condition": extract_from_table(table, "Ingatlan állapota"),
        "Year": extract_from_table(table, "Építés éve"),
        "Comfort": extract_from_table(table, "Komfort"),
        "Energy": extract_from_table(table, "Energiatanúsítvány"),
        "Floor": extract_from_table(table, "Emelet"),
        "Buildingfloors": extract_from_table(table, "Épület szintjei"),
        "Elevator": extract_from_table(table, "Lift"),
        "Height": extract_from_table(table, "Belmagasság"),
        "Heating": extract_from_table(table, "Fűtés"),
        "UtilityCost": extract_from_table(table, "Rezsiköltség"),
        "Disability": extract_from_table(table, "Akadálymentesített"),
        "Bathroom": extract_from_table(table, "Fürdő és WC"),
        "Position": extract_from_table(table, "Tájolás"),
        "View": extract_from_table(table, "Kilátás"),
        "Teras": extract_from_table(table, "Erkély"),
        "Garden": extract_from_table(table, "Kertkapcsolatos"),
        "Topfloor": extract_from_table(table, "Tetőtér"),
        "Parking": extract_from_table(table, "Parkolás"),
        "ParkingCost": extract_from_table(table, "Parkolóhely ára"),
        "Airconditioning": extract_from_table(table, "Légkondicionáló"),
        "URL": url,
        "District": district,
        "InactiveAd": True if page.find("div", class_="is-ad-inactive") else False,
        "LongDescription": (page.find("div", class_="long-description") or DEFAULT_TAG).text,
        "id": (page.find("b", class_="listing-id") or DEFAULT_TAG).text,
        "_id": {"id": (page.find("b", class_="listing-id") or DEFAULT_TAG).text, "time": time_identifier}
    }