from utils import num2roman

DOMAIN = "https://ingatlan.com"


def generate_parent_pages():
    return [{
        'url': DOMAIN + "/lista/elado+lakas+" + num2roman(district) + "-ker",
        'district_number': district
    } for district in range(1, 24)]
