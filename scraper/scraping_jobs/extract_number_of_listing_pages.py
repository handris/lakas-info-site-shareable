from bs4 import BeautifulSoup


def extract_number_of_listing_pages(html_content):
    soup = BeautifulSoup(html_content, "html.parser")

    return int(soup.select(".pagination__page-number")[0].text.split(' ')[3])
