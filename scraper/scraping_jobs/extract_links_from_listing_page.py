from bs4 import BeautifulSoup


DOMAIN = "https://ingatlan.com"


def extract_links_from_listing_page(html_content):
    current_page = BeautifulSoup(html_content, 'html.parser')
    return [DOMAIN + listing["href"] for listing in current_page.findAll("a", {"class": "listing__link"})]
