from os import environ
from sys import exit
import logging


def check_if_env_vars_are_set(required_environment_variables):
    not_set_variables = [var for var in required_environment_variables if var not in environ]

    if not_set_variables:
        logging.error("{} environment variable(s) need to be set. Exiting.".format(", ".join(not_set_variables)))
        exit(1)
