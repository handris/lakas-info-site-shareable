import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient
from time import sleep
import os

mongo = MongoClient("mongodb://{}:{}@207.154.246.90".format(os.getenv('MONGO_USER'), os.getenv('MONGO_PASSWORD')))

if __name__ == '__main__':
    data = []

    for district in range(1, 24):
        sleep(1)
        response = requests.get('https://utca-terkep.info/budapest-{}-kerulet/'.format(district))
        print(response.status_code)

        current_page = BeautifulSoup(response.content, 'html.parser')

        table_rows = current_page.find_all('tr')[1:]

        for row in table_rows:

            if row.find_all('td'):
                data.append({
                    'street': row.find_all('td')[0].find('a').text,
                    'zip_code': row.find_all('td')[1].text,
                    'district': district
                })

    mongo.real_estates.street_zip_mappings.insert_many(data)
