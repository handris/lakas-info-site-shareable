import os
from pymongo import MongoClient
import re

mongo = MongoClient("mongodb://" + os.getenv('MONGO_USER') + ":" + os.getenv('MONGO_PASSWORD') + "@lakas.info")

if __name__ == '__main__':

    ratios_file = open('unique_addresses/ratios.txt', 'w')
    for district in range(1, 24):
        documents = mongo.real_estates.real_estate_records.find({
            "District": district,
            "_id.time": "2019_13"
        })

        iteration_count = 0
        match_count = 0
        not_matched_set = set()

        for document in documents:
            iteration_count += 1
            if document.get('Address') is not None:

                match = re.search(r'(\d+). kerület, ((?:[A-Z][\w-]+ )+)([a-z-]*)(.*)', document.get('Address'))
                if match:
                    district, street, *_ = match.groups()

                    mapping_zip = mongo.real_estates.street_zip_mappings.find_one({
                        "district": int(district),
                        "street": {"$regex": '^{}'.format(street.strip())}
                    })

                    if mapping_zip is not None:
                        print(mapping_zip)
                        match_count += 1
                    else:
                        print("----------------no match--------------------")
                        not_matched_set.add("{}: {}, {}".format(document.get('Address'), district, street))

        district_ratio = "Matched ratio for district {}: {}".format(district, match_count / iteration_count)
        print(district_ratio)
        ratios_file.write(district_ratio)
        ratios_file.write("\n")

        address_file = open('unique_addresses/{}-district.txt'.format(district), 'w')
        for address in not_matched_set:
            address_file.write(address)
            address_file.write("\n")
        address_file.close()
