import os
from pymongo import MongoClient

mongo = MongoClient("mongodb://" + os.getenv('MONGO_USER') + ":" + os.getenv('MONGO_PASSWORD') + "@lakas.info")

if __name__ == '__main__':

    documents = mongo.real_estates.real_estate_records.find({
        "District": 1,
        "_id.time": "2019_12",
    })

    for document in documents:
        query = {
            "District": 1,
            "_id.time": "2019_12",
            "_id.id": document.get("_id").get("id")
        }
        number = mongo.real_estates.real_estate_records.find(query).count()
        print("_________________________________________________________")
        print(number)

        if number > 1:
            mongo.real_estates.real_estate_records.delete_one(query)

            number = mongo.real_estates.real_estate_records.find(query).count()
            print(number)
