import logging

from app.errors import blueprint

logger = logging.getLogger(__name__)


@blueprint.route('/healthcheck')
@blueprint.route('/healthCheck')
def health_check():
    return '{"success": true}'

