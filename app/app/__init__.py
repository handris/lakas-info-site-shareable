import logging

from config import Config
from flask import Flask


def create_app(config_class=Config):
    app = Flask(__name__)

    app.config.from_object(config_class)

    from app.errors import blueprint as errors_blueprint
    app.register_blueprint(errors_blueprint, url_prefix='/api')

    from app.machine_learning import blueprint as machine_learning_blueprint
    app.register_blueprint(machine_learning_blueprint, url_prefix='/api')

    logging.basicConfig(level=logging.INFO)

    return app
