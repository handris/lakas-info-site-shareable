import logging
from flask import Blueprint, jsonify, request, abort, Response, make_response
import pandas as pd
import numpy as np
import pickle

from app.machine_learning import blueprint
from app.machine_learning.preprocessing import preprocessData
from app.machine_learning.validator import validate_optional_inputs

logger = logging.getLogger(__name__)

model_version = 'v21'
loaded_model = pickle.load(open('app/machine_learning/models/Model_' + model_version + '.sav', 'rb'))


@blueprint.route('/predict-from-inputs', methods=['POST'])
@validate_optional_inputs()
def predict_from_inputs(optional_data):
    print("____________________")
    print("____________________")
    print("____________________")
    print(request.json)
    if not request.json:
        logger.info("Received a request with non-json data")
        return Response("Content type must be json", 400)

    logger.info("Received a request with the following data: {}".format(request.json))

    if 'district' not in request.json:
        return Response("Request does not contain address field", 400)
    if 'area' not in request.json:
        return Response("Request does not contain area field", 400)
    if 'rooms' not in request.json:
        return Response("Request does not contain rooms field", 400)
    if 'half_rooms' not in request.json:
        return Response("Request does not contain half_rooms field", 400)
    if 'floor' not in request.json:
        return Response("Request does not contain floor field", 400)
    if 'teras' not in request.json:
        return Response("Request does not contain teras field", 400)

    try:
        district = int(request.json['district'])
        if not (1 <= district <= 23):
            raise ValueError()
    except ValueError:
        return Response("District field must be an integer in the range of 1 to 24", 400)

    try:
        area = int(request.json['area'])
        if not (1 <= area <= 350):
            raise ValueError()
    except ValueError:
        return Response("Area field must be an integer in the range of 1 to 350", 400)

    try:
        rooms = int(request.json['rooms'])
        if not (1 <= rooms <= 10):
            raise ValueError()
    except ValueError:
        return Response("Rooms field must be an integer in the range of 1 to 10", 400)

    try:
        half_rooms = int(request.json['half_rooms'])
        if not (0 <= half_rooms <= 8):
            raise ValueError()
    except ValueError:
        return Response("Half_rooms field must be an integer in the range of 0 to 8", 400)

    try:
        floor = float(request.json['floor'])
        if floor not in list(range(-1, 12))+[0.5]:
            raise ValueError()
    except ValueError:
        return Response("Floor field must be an integer in the range of -1 to 11 or 0.5", 400)

    try:
        teras = float(request.json['teras'])
        if not (0 <= teras <= 100):
            raise ValueError()
    except ValueError:
        return Response("Teras field must be an integer in the range of 1 to 100", 400)

    prepocessing_input = create_preprocessing_input(district, area, rooms, half_rooms, floor, teras, optional_data)

    df_processed = preprocessData(prepocessing_input)
    del df_processed['Price_clean']

    prediction = loaded_model.predict(df_processed)[0]

    logger.info("Made prediction {} from data: {}".format(prediction, request.json))

    return jsonify({'price': prediction})


def create_preprocessing_input(district, area, rooms, half_rooms, floor, teras, optional_data):
    return pd.DataFrame([{
        "District": str(district),
        "Area_clean": area,
        "Rooms_whole": rooms,
        "Rooms_half": half_rooms,
        "Floor_clean": floor,
        "Teras_clean": teras,
        "Airconditioning": optional_data['airconditioning'],
        "Comfort": optional_data['comfort'],
        "Condition": optional_data['condition'],
        "Elevator": optional_data['elevator'],
        "Heating": optional_data['heating'],
        "Height": optional_data['height'],
        "Parking": optional_data['parking'],
        "View": optional_data['view'],
        "Year": optional_data['year']
    }])

