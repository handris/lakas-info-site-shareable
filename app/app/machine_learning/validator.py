import logging
from functools import wraps

from flask import request, Response


logger = logging.getLogger(__name__)

schema = {
    'year': ['Before_1950', 'After_2017', '1950-1980', '2001-2010', '1981-2000', '2011-2017', 'NA'],
    'condition': ['good', 'renovated', 'newlike', 'mediocre', 'bad', 'newlybuilt', 'unfinished', 'NA'],
    'comfort': ['full', 'double', 'luxury', 'normal', 'half', 'lack', 'NA'],
    'elevator': ['yes', 'no', 'NA'],
    'height': ['below3m', 'over3m', 'NA'],
    'heating': ['gas_central', 'gas_convector', 'central_individual',
                'district', 'central', 'district_individual', 'electric',
                'heat_pump', 'renewable', 'gas_underfloor', 'fan_coil', 'masonry_heater', 'underfloor', 'NA'],
    'view': ['street', 'backyard', 'garden', 'panoramic', 'NA'],
    'parking': ['public_fee', 'public', 'public_free', 'garage_canbuy', 'yard_included', 'garage_included',
                'garage', 'standalone_canbuy', 'garage_mustbuy', 'standalone_included', 'yard', 'yard_canbuy',
                'yard_rent', 'garage_rent', 'standalone', 'standalone_mustbuy', 'yard_mustbuy', 'NA'],
    'airconditioning': ['yes', 'no', 'NA']
}

default_values = {
    'year': 'NA',
    'condition': 'NA',
    'comfort': 'NA',
    'elevator': 'NA',
    'height': 'NA',
    'heating': 'NA',
    'bathroom': 'NA',
    'view': 'NA',
    'parking': 'NA',
    'airconditioning': 'NA',
}


def validate_optional_inputs():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            optional_data = {}

            if not request.json:
                return Response("Content type must be json", 400)

            for input_field, valid_inputs in schema.items():
                if input_field in request.json:
                    if request.json[input_field] in valid_inputs:
                        optional_data[input_field] = request.json[input_field]
                    else:
                        logger.info("Recieved a request with invalid optional data {}, invalid field name: {}"
                                    .format(request.json, input_field))
                        return Response("Valid values for {} are {}".format(input_field, valid_inputs), 400)
                else:
                    optional_data[input_field] = default_values[input_field]

            return f(optional_data=optional_data, *args, **kwargs)
        return decorated_function
    return decorator
