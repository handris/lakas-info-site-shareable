from flask import Blueprint

blueprint = Blueprint('machine_learning', __name__)

from app.machine_learning import routes