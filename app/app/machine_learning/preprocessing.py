import pandas as pd
import re
import numpy as np
from scipy.special import boxcox1p
from sklearn.ensemble import GradientBoostingRegressor


def preprocessData(df_raw):
    '''Function to preprocess data. Takes pandas df and returns pandas df.'''
    model_version = 'v21'

    # get necessary files
    bxc_feats = [line.rstrip('\n') for line in
                 open('app/machine_learning/models/skewness_' + model_version + '.txt', encoding="ISO-8859-1")]
    all_feats = [line.rstrip('\n') for line in
                 open('app/machine_learning/models/features_' + model_version + '.txt', encoding="ISO-8859-1")]

    # select features which are used
    df_main = df_raw[['Airconditioning', 'Area_clean', 'Comfort', 'Condition', 'District', 'Elevator',
                      'Floor_clean', 'Heating', 'Height', 'Parking', 'Rooms_whole', 'Rooms_half', 'Teras_clean', 'View',
                      'Year']].copy()

    # FEATURE ENGINEERING
    df_main['Area_clean-2'] = df_main['Area_clean'] ** 2
    df_main['Rooms_wholexhalf'] = df_main['Rooms_half'] * df_main['Rooms_whole']

    # PROCESSING
    df_main = pd.get_dummies(df_main)

    cur_feats = df_main.columns.tolist()

    drp_feats = [item for item in cur_feats if item not in all_feats]
    add_feats = [item for item in all_feats if item not in cur_feats]

    df_main = df_main.drop(drp_feats, axis=1)

    for feat in add_feats:
        df_main[feat] = np.NaN
        df_main[feat].fillna(0, inplace=True)

    # APPLY BOXCOX TRANSFORMATION
    lam = 0.15
    for feat in bxc_feats:
        df_main[feat] = boxcox1p(df_main[feat], lam)

    # REORDER COLUMNS
    df_main = df_main[all_feats]

    return df_main


