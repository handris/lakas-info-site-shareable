include .env

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'


up:       ##             Runs application in development mode
	@docker-compose -f docker-compose.development.yml up --build --renew-anon-volumes

up-prod: ##              Runs application in production mode while building Docker images locally
	@docker-compose -f docker-compose.local-production-build.yml up --build

up-pull-prod: ##         Runs application by pulling images from Docker Hub
	@docker-compose -f docker-compose.production.yml up

db:         ##           Connects to the remote mongo instance
	@mongo lakas.info --username $(MONGO_USER) --password $(MONGO_PASSWORD) --authenticationDatabase admin

db-local:             ## Connects to the local mongo instance
	@mongo localhost --username $(MONGO_USER) --password $(MONGO_PASSWORD) --authenticationDatabase admin

dump:                 ## Create a dump from the remove mongo instance
ifdef file
	@echo 'Creating mongo dump at ./mongo/dump/${file}'
else
	@echo "Missing parameter: file. Exiting..."
	exit 1
endif
	@mongodump --host lakas.info --username $(MONGO_USER) --password $(MONGO_PASSWORD) --authenticationDatabase admin -vvvvvv -o ./mongo/dump/${file}
